# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 12:17:17 2014

@author: dgevans
"""
import numpy as np
import pricing as AP
import pandas as pd
import pandas.io.data as web
import datetime


##################
# Question 1     #
##################
S = 5
Pi = 0.0125*np.ones((5,5))
Pi += np.diag(0.95-0.0125*np.ones(5))
#construct lambda
lamb = np.array([1.025,1.015,1.,0.985,0.975])
#gamma
gamma = 2.
#beta
beta = 0.96

#part a.
pf, Rf =   AP.riskFreePrice(Pi,lamb,gamma,beta)
print ("Risk Free Bond Price and return: ")
print (pf)
print (Rf)


#part b
nu,R = AP.stockPrice(Pi,lamb,gamma,beta)
print ("Stock price and return: ")
print (nu)
print (R)



#part c
zeta = 1.
pc,Rc = AP.consolPrice(Pi,lamb,gamma,beta,zeta)
print ("Consol Bond Price and Return: ")
print (pc)
print (Rc)


#part d
p_s = 30.
w = AP.callOption(Pi,lamb,gamma,beta,zeta,p_s,20)
print ('Price of Call Option: ')
print (w)


##################
# Question 1     #
##################

#part a
start = datetime.datetime(1955, 1, 1)
end = datetime.datetime(2013, 1, 1)
df = web.DataReader("PCECC96", "fred", start, end)

df['C'] = df['PCECC96']
df['C_'] = df.C.shift() 
df['c_growth'] = df.C/df.C_ 
df.drop(df.index[0],inplace=True)
df['s'] = pd.qcut(df.c_growth,10,labels=False)
#part b
lambq = df.groupby('s').c_growth.mean()
Pi_data = np.ones(10,10)*.1

#Rs[0]= Rs[shist[0:-1],shist[1:]]
#part c
gamma_vec = np.linspace(0,20,50)
K = AP.estimateEquityPremium()