# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 11:52:11 2014

@author: dgevans
"""
import numpy as np

def riskFreePrice(Pi,lamb,gamma,beta):
    
    '''
    Computes the price of a risk free bond
    
    Inputs
    -------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    
    Returns
    ---------
    * pf - Price of the risk free bond for each state of the world (length S array)
    * Rf - Return on the risk free bond for each state of the world
    '''
    E_ul = Pi.dot(lamb**(-gamma))
    pf = beta* E_ul
    Rf = 1/pf
    return pf, Rf
    
    pass

def stockPrice(Pi,lamb,gamma,beta):
    '''
    Computes the price to dividend ratio nu, and return on stock
    
    Inputs
    --------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    
    Returns
    ----------
    * nu - Price to dividend ratio for each state of the world
    * Rs - Return on the stock from state s to sprime
    '''
    S = len(lamb)
    ptilde = Pi*lamb**(1-gamma)
    temp = np.linalg.inv(np.eye(S)-beta*ptilde)
    nu = beta*(temp.dot(ptilde.dot(np.ones(S))))
    
    R= np.zeros((S,S))
    R[0] = (nu+1)*lamb/(nu[0])
    for s in range(1,S):
      R[s]=  (nu+1)*lamb/(nu[s])
        
    return nu, R
    
    
    
    pass
    
def consolPrice(Pi,lamb,gamma,beta,zeta):
    '''
    Computes price of a consol bond with coupon zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    * zeta - coupon (float)
    
    Returns
    ---------
    * pc - price of the consol in each state (length S array)
    * Rc - Return on the consol in each state (length S array)
    '''
    
    S = len(lamb)
    ptilde = Pi*lamb**(-gamma)
    temp = np.linalg.inv(np.eye(S)-beta*ptilde)
    pc = beta*(temp.dot(zeta*ptilde.dot(np.ones(S))))
    Rc= np.zeros((S,S))
    Rc[0] = (pc+1)/(pc[0])
    for s in range(1,S):
      Rc[s]=  (pc+1)/(pc[s])
    return pc,Rc
    pass
def optionTMap(Pi,lamb,beta,gamma,pStrike,what,zeta):
    pc, Rc = consolPrice(Pi,lamb,gamma,beta,zeta)
    Pi_tilde= Pi*(lamb**-gamma)
    PS = pc-pStrike
    wne = beta*Pi_tilde.dot(what)
    return np.maximum(PS,wne)
     

def callOption(Pi,lamb,gamma,beta,zeta,pStrike,T, epsilon = 1e-8):
    '''
    Computes price of a call option on a consol bond with payoff zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * zeta - coupon (float)
    * pStrike - strike price (float)
    * T - length of option (can be inf)
    * epsilon - tolerance for infinite horizon problem (float) (bonus question)
    
    Returns
    --------
    * w - price of option in each state of the world
    '''
    S=len(lamb)
    w={}
    w[0]=np.zeros(S)
    for t in range(0,T):
        w[t+1] = optionTMap(Pi,lamb,beta,gamma,pStrike,w[t],zeta)
    return w[T]
   
    
   
    pass

    
    
def estimateEquityPremium(Pi_data,lambda_data,gamma,beta,sHist):
    '''
    Estimates the equity premium
    
    Inputs:
    --------
    * Pi_data : estimated transition matrix
    * lambda_data: estimated consumption growth
    * gamma : degree of risk aversion
    * beta : discount rate 
    * sHist : sample path of states
    
    Returns:
    -----------
    * EP : estimate of equity premium
    '''
    prf,rrf = riskFreePrice(Pi_data,lamda_data,gamma,beta)
    ps,rs = stockPrice(Pi_data,lamda_data,gamma)
    return prf, ps     
    pass
    
